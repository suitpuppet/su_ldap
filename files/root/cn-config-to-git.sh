#!/usr/bin/bash -e

# Dump the cn=config configuration, generate a gomplate template for thh cloud replicas, and
# push both cn-config.ldif and cn-config.ldif.tmpl to the provided git repository.

# Error out if commit message is not set
COMMIT_MSG=$1
COMMIT_MSG=${COMMIT_MSG:?'COMMIT_MSG not set'}

CONFIG_FILE='cn-config.ldif'
CONFIG_DUMP_DIR=$(mktemp -d /tmp/configdump.XXXX)
GIT_REPO_DIR=$(mktemp -d /tmp/git.XXXX)
GIT_REPO_CONFIG='/etc/ldap-git-config/config'
USERNAME=${SUDO_USER:-$USER}
USERID=$(echo $USERNAME | sed 's/.root//')

GIT_REPO_URL=$(grep 'git_url:' ${GIT_REPO_CONFIG} | sed 's/^.*: //')
GIT_REPO_SSHKEY=$(grep 'ssh_key:' ${GIT_REPO_CONFIG} | sed 's/^.*: //')
LDAP_ROLE=$(grep 'git_url:' ${GIT_REPO_CONFIG}  | cut -d'/' -f2 -)

# Change master config to cloud shadow master config
export OLC_SYNCREPL='olcSyncrepl: {0}rid=100 provider=ldap://{{.Env.LDAP_MASTER}}:389 bindmethod=sasl timeout=0 network-timeout=0 saslmech=gssapi realm=stanford.edu keepalive=0:0:0 starttls=no filter="(objectclass=*)" searchbase="dc=stanford,dc=edu" logfilter="(&(objectClass=auditWriteObject)(reqResult=0))" logbase="cn=accesslog" scope=sub schemachecking=on type=refreshAndPersist retry="60 +" syncdata=accesslog'
export OLC_UPDATEREF='olcUpdateRef: {{.Env.LDAP_MASTER}}'
export LDAP_DB='dn:\s+olcDatabase=\{2\}mdb\,cn=config'

# Need this for git operations
export GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ${GIT_REPO_SSHKEY}"

function abort () {
    echo "$msg"
    exit 1
}

# Dump the cn=config tree to a file.
function dump_config () {
    # Dump the file.
    if ! /usr/sbin/slapcat -F /etc/ldap/slapd.d -b cn=config  -o ldif-wrap=no -l $CONFIG_DUMP_DIR/$CONFIG_FILE 2> /tmp/err && ! grep -q PROXIED /tmp/err;
    then
      msg=$(cat /tmp/err)
      abort
    fi
}

function git_publish () {
    cd ${GIT_REPO_DIR}
    git add ${CONFIG_FILE} ${CONFIG_FILE}.tmpl
    git -c user.name=${USERNAME} \
	    -c user.email=${USERID}@stanford.edu commit -m "${COMMIT_MSG}"
    git push 
}

# Generate GKE template cn=config
function update_config_tmpl () {
    echo "#!gomplate" > ${CONFIG_FILE}.tmpl
    if [[ "${LDAP_ROLE}" = "ldap-replica-config" ]]; then
        perl -p -e 's#^olcSyncrepl:\s+\{0\}rid=\d+\s+#olcSyncrepl: \{0\}rid={{.Env.RID}} #' ${CONFIG_FILE} >> ${CONFIG_FILE}.tmpl
        perl -pi -e 's#provider=ldap://.*:389#provider=ldap://{{.Env.MASTER}}:389#' ${CONFIG_FILE}.tmpl
        perl -pi -e 's#^olcUpdateRef:\s+ldap://.*$#olcUpdateRef: ldap://{{.Env.MASTER}}#' ${CONFIG_FILE}.tmpl
    elif [[ "${LDAP_ROLE}" = "ldap-master-config" ]]; then
        perl -p -e '/$ENV{LDAP_DB}/ and $_.="$ENV{OLC_SYNCREPL}\n$ENV{OLC_UPDATEREF}\n"' ${CONFIG_FILE} >> ${CONFIG_FILE}.tmpl
    else
        abort "Unknown ldap role: ${LDAP_ROLE}"
    fi
}

# Main
dump_config
git clone $GIT_REPO_URL ${GIT_REPO_DIR}

cd ${GIT_REPO_DIR}
cp ${CONFIG_DUMP_DIR}/${CONFIG_FILE} $CONFIG_FILE 
sed -i'' 's/^modifiersName/#modifiersName/' ${CONFIG_FILE}
if [[ "$(git status -s)" != "" ]]; then
    update_config_tmpl
    git_publish
else
   echo "No changes"
fi
exit 0
