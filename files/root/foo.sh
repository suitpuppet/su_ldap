#!/bin/bash

export OLC_SYNCREPL='olcSyncrepl: {0}rid=100 provider=ldap://{{.Env.LDAP_MASTER}}:389 bindmethod=sasl timeout=0 network-timeout=0 saslmech=gssapi realm=stanford.edu keepalive=0:0:0 starttls=no filter="(objectclass=*)" searchbase="dc=stanford,dc=edu" logfilter="(&(objectClass=auditWriteObject)(reqResult=0))" logbase="cn=accesslog" scope=sub schemachecking=on type=refreshAndPersist retry="60 +" syncdata=accesslog'
export OLC_UPDATEREF='olcUpdateRef: {{.Env.LDAP_MASTER}}'
export LDAP_DB='dn:\s+olcDatabase=\{2\}mdb\,cn=config'

#perl -e 'print "you are logged in as $ENV{OLC_SYNCREPL}\n";'
perl -pi -e '/$ENV{LDAP_DB}/ and $_.="$ENV{OLC_SYNCREPL}\n$ENV{OLC_UPDATEREF}\n"' foo.txt
