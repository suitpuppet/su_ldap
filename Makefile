# Shared makefile.mk by <env>/Makefile
# get the realpath of GCP_ROOT_DIR and COMMON
MAKEFILE_DIR := $(dir $(firstword $(MAKEFILE_LIST)))
GCP_ROOT_DIR := $(realpath ${MAKEFILE_DIR}../..)

# include env vars
include ${GCP_ROOT_DIR}/env.sh
include ${MAKEFILE_DIR}/env.mk

export

# FRAMEWORK SYNC
ifeq ($(MAKELEVEL),0)
    _ := $(shell >&2 echo)
	ifneq ($(wildcard ${FRAMEWORK_DIR}/.git/),)
		_ := $(shell >&2 echo Updating PS cloud framework from Git into ${FRAMEWORK_DIR}...)
		_ := $(shell cd ${FRAMEWORK_DIR}; git pull)
	else
		_ := $(shell >&2 echo Updating PS cloud framework in ${FRAMEWORK_DIR}...)
		_ := $(shell mkdir -p ${FRAMEWORK_DIR} && curl --retry 3 -s https://storage.googleapis.com/${FRAMEWORK_BUCKET}/framework.tar.gz?random=$$(date +%s) | tar -xzf - -C ${FRAMEWORK_DIR})
		_ := $(shell >&2 echo - framework version: $$(cat ${FRAMEWORK_DIR}/sha.txt))
	endif
endif
# END FRAMEWORK SYNC

include ${FRAMEWORK_DIR}/makefile_parts/shared.mk
include ${FRAMEWORK_DIR}/makefile_parts/gitlab.mk
include ${FRAMEWORK_DIR}/makefile_parts/vault.mk

