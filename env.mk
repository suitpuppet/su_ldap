# GitLab configuration
GITLAB_SERVER=https://code.stanford.edu
GITLAB_REPO=suitpuppet/su_ldap
SLACK_WEBHOOK_PATH=${SEC_PATH}/common/slack/gitlab-integration
SLACK_GITLAB_CHANNEL=authnz-git-commits
SLACK_CICD_CHANNEL=authnz-build
