# Basic configuration: /etc/ldap/ldap.conf, /etc/default/slapd, et al.

# Install the OpenLDAP Debian packages.
#
# $distribution: A valid Debian distribution. This can
#   be the usual ones, such as "jessie", "stretch", "buster", "sid".
#   It can also be one of the stanford special ones, e.g.,
#   "stretch-acs-dev", "stretch-acs-prod", etc.
#
#  #########
#  Example 1
#
#  class {'su_ldap::openldap_install':
#    distribution => 'stretch'
#  }
#
#  This will install the stock OpenLDAP packages from the stretch distribution.
#
#
#  #########
#  Example 2:
#
#  class {'su_ldap::openldap_install':
#    distribution => 'stretch-openldap-dev'
#  }
#
# This will install the OpenLDAP packages from the special stretch-acs-dev
# distribution that resides in the "stanford" repository.

class su_ldap::openldap_install (
  $distribution = undef,
) {

  # Only need to do something if $distribution is defined.

  if ($distribution) {

    # Pin some OpenLDAP packages.
    file { '/etc/apt/preferences.d/ldap':
      content => template('su_ldap/etc/apt/preferences.d/ldap.erb'),
      notify  => Exec['ldap aptitude update'];
    }

    # What about special sasl? LATER.

    # Update aptitude.
    exec { 'ldap aptitude update':
      path        => '/usr/bin:/usr/sbin:/bin',
      command     => 'aptitude update',
      refreshonly => true,
    }

    # Install slapd.
    package { 'slapd':
      ensure  => installed,
      require => [
        File['/etc/default/slapd'],
        File['/etc/apt/sources.list.d/ldap.list'],
        File['/etc/apt/preferences.d/ldap'],
        File['/etc/apt/preferences.d/cyrus-sasl']

    }
}
