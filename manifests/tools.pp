## don't use this one - use s_ldap::tools

## built based on previous
class su_ldap::tools (
  $env                  = undef,
)
{
  ## Nagios monitoring

  ## Firewall rules

  ## syncs from - to etc

  ## Keytabs
  # Create primary keytab file.  Probably don't need to do this
  # but it makes the specification more consistent.
  base::wallet { "host/$fqdn":
    path    => '/etc/krb5.keytab',
    owner   => 'root',
    primary => true,
    ensure  => present,
  }
  # Add the service/ldap keytab to the primary keytab.
  base::wallet { 'service/ldap':
    path    => '/etc/krb5.keytab',
    primary => false,
    require => Base::Wallet["host/$fqdn"],
    ensure  => present,
  }

  ## ACLS that need for ldap alias maint and posixgroup

  file {
    '/etc/remctl/acl/tools':
        mode   => '0644',
        source => 'puppet:///modules/su_ldap/etc/remctl/acl/tools';
    '/etc/remctl/acl/its-rc-posix-gm':
        mode   => '0644',
        source => 'puppet:///modules/su_ldap/etc/remctl/acl/its-rc-posix-gm';
  }

  # keytab acls the thing needs access to
  #  wallet acl add ldap-wg-api-dev ...
  # or ldap-wg-api
  #  wallet acl add idg-ldapadmin-db-ldaplog ...
  #  wallet acl add group/ldap-hosts ...
  #



  ## Remctl

  ## krb5 ticket cache

  ## ldap-reports

  ## bundle remctl endpoint

  ## Non-sync cron jobs

}
