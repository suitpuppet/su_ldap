## Class for ldap backups to AWS and GCP

class su_ldap::backups(
  #
  $env  = undef,
  #
) {

  # Add the service/ldap keytab to the above primary keytab. This requires
  # for backups.
  if ( $env == 'prod') {
    $service_name = "service/ldap"
    $master_name = "ldap/ldap-master.stanford.edu"
  } else {
    $service_name = "service/ldap-${env}"
    $master_name = "ldap/ldap-${env}master.stanford.edu"
  }

  base::wallet { $service_name:
    ensure  => present,
    path    => '/etc/krb5.keytab',
    primary => false,
    require => Base::Wallet["host/${::fqdn}"],
  }

  # Note: ldap/ldap-${env}master needs to come after service/ldap-env
  base::wallet { $master_name:
    ensure  => present,
    path    => '/etc/krb5.keytab',
    primary => false,
    type    => 'keytab',
    require => Base::Wallet["host/${::fqdn}"],
  }

  ## This section is the sync of the daily backups to AWS S3, only done for the masters
  # first, put awscli on the host - you don't have to use python if you have puppet
  # package { 'awscli': ensure => present }

  # Module for google cloud SDK Installation
  include package_google_cloud_sdk

  # cron jobs we care about
  file {
    '/etc/cron.d/master-audit':
      mode    => '0644',
      source  => 'puppet:///modules/su_ldap/etc/cron.d/master-audit';
    '/etc/cron.d/master-restart':
      mode    => '0644',
      content => template('su_ldap/etc/cron.d/master-restart.erb');
    '/etc/cron.d/remctl-acl-mais-staff':
      source => 'puppet:///modules/su_ldap/etc/cron.d/remctl-acl-mais-staff';
  }

  # We want a newsyslog task to rotate the LDAP _data_ (as opposed to the
  # LDAP logs) into a local directory. If this is the production master,
  # we want to rotate the data into AFS as well.
  #
  $save_to_afs  = true
  file { '/etc/newsyslog/master':
    ensure  => file,
    mode    => '0644',
    content => template('su_ldap/etc/newsyslog/master.erb'),
    require => File['/etc/newsyslog'],
  }

  ## Directory where we put GCP credentials
  file {'/etc/ldapadmin':
    ensure => directory;
  }

  # third, pick your bucket and credentials
  ##case $env {
  #  'prod' : {
  #    $s3bucket = 's3://060553448345-authnz-prod-ldapdumps/'
  #    $s3creds  = 'config/its-idg/aws-s3-ldap-prod/access-keys'
  #  }
  #  default: {
  #    $s3bucket = 's3://608913223100-authnz-x-ldapdumps/'
  #    $s3creds  = 'config/its-idg/aws-s3-ldap-non-prod/access-keys'
  #  }
  #}

  ##  $wallet_name = "service/its-idg/git-cn-config-sshkey/ldap-${suffix}"
  #base::wallet { $s3creds:
  #  ensure  => 'present',
  #  path    => '/etc/ssl/private/s3bucket-keys',
  #  type    => 'file',
  #  owner   => 'root',
  #  group   => 'root',
  #  mode    => '0640',
  #}

  # Pickup right environment credentials from wallet
  ## Production ldapuser serviceaccount
  $gcs_credentials_prod  = 'password/its-idg/gcs/prod-ldapuser_serviceaccount'

  ## Stage ldapuser service account
  $gcs_credentials_stage = 'password/its-idg/gcs/stage-ldapuser_serviceaccount'

  # get ldapuser service account
  base::wallet { $gcs_credentials_stage:
    ensure  => 'present',
    path    => '/etc/ldapadmin/gcs_stage_ldapuser_serviceaccount.json',
    type    => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
  }

  if ( $env == 'prod') or ( $env == 'uat' ) {
     base::wallet { $gcs_credentials_prod:
       ensure  => 'present',
       path    => '/etc/ldapadmin/gcs_prod_ldapuser_serviceaccount.json',
       type    => 'file',
       owner   => 'root',
       group   => 'root',
       mode    => '0640',
     }
  }


}
