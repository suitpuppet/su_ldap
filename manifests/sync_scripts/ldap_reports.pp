# The ldap-reports service.
#
# See also https://ikiwiki.stanford.edu/service/ldap/sync-scripts/
#
# NOTE: Normally installed only on prod and UAT.

class su_ldap::sync_scripts::ldap_reports (
  $ensure           = undef,
  $env              = undef,
  $ldap_master_fqdn = undef,
  $addresses        = [],
  $basedir          = '/etc/ldap-reports',
){

  ## This spackage just has ldap-reports stuff, and DOES NOT require slapd
  # /usr/bin/ldap-report
  # /usr/bin/ldap-getaffiliation
  # /usr/bin/ldap-get-first-last
  # /usr/bin/ldap-summary
  # /usr/share/perl5/Stanford/LDAP/FilterList.pm
  package { 'stanford-ldap-tools': ensure  => installed }

  ## Configuration file

  file { $basedir:
    ensure => directory,
  }

  file { "${basedir}/som-mail-forwards-non-stanford.conf":
    ensure  => $ensure,
    content => template('su_ldap/etc/ldap-reports/som-mail-forwards-non-stanford.conf.erb'),
  }

  ## Cron job
  file { '/etc/cron.d/ldap-reports':
    ensure  => $ensure,
    content  => template('su_ldap/etc/cron.d/ldap-reports.erb'),
    require => File["${basedir}/som-mail-forwards-non-stanford.conf"],
  }

}
