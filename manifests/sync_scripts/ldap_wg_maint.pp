# The ldap-wg-maint service.
#
# The ldap-wg-maint service looks at the suSeasLocal attribute and, based
# on its value, updates one of the itservices e-mail workgroups in
# Workgroup Manager.
#
# See also https://ikiwiki.stanford.edu/service/ldap/sync-scripts/
#
# Moving from 'conf' to 'yaml' configuration file format.
#
# For information on the $enable_seclevel1 parameter see the
# man page for ldap-wg-maint.
#
# NOTE: Normally installed only on prod and UAT.

class su_ldap::sync_scripts::ldap_wg_maint (
  Enum['present', 'absent']
         $ensure            = undef,
  String $env               = undef,
  #
  Enum['conf', 'yaml']
         $conf_file_format  = 'conf',
  #
  String $ticket_file_path  = undef,
  String $ldap_master_fqdn  = undef,
  String $basedir           = '/etc/ldapadmin',
  #
  Boolean $enable_seclevel1 = false,
){

  # Do we want the service to be running or stopped?
  if ($ensure == 'present') {
    $service_status = 'running'
  } elsif ($ensure == 'absent') {
    $service_status = 'stopped'
  } else {
    fail("ensure must be one 'present' or 'absent'")
  }

  # (Note: $wg_host is used in a template file.)
  #
  # ALL non-production environmements use the same MAIS credentials
  # (probably should have named it "wg-api-nonprod.ldap.stanford.edu";
  # maybe next time).
  if ($env == 'prod') {
    $wg_wallet = 'ssl-key/ldap.stanford.edu/wg-api'
    $wg_cert   = 'wg-api.ldap.stanford.edu'
    $wg_host   = 'workgroupsvc.stanford.edu'
  } else {
    $wg_wallet = 'ssl-key/ldap-dev.stanford.edu/wg-api'
    $wg_cert   = 'wg-api-dev.ldap.stanford.edu'
    $wg_host   = "workgroupsvc-${env}.stanford.edu"
  }

  ## Workgroup API credentials
  # Credential for connection to the workgroup api web service
  wallet { $wg_wallet:
    ensure => $ensure,
    path   => "/etc/ssl/private/${wg_cert}.key",
    type   => 'file',
  }
  file { "/etc/ssl/certs/${wg_cert}.pem":
    ensure => $ensure,
    source => "puppet:///modules/su_ldap/etc/ssl/certs/${wg_cert}.pem",
  }

  ## Configuration file (old-style; to go away eventually)
  file { "${basedir}/ldap-wg-maint.conf":
    ensure  => $ensure,
    content => template('su_ldap/etc/ldapadmin/ldap-wg-maint.conf.erb'),
  }
  ## Configuration file (new style)
  file { "${basedir}/ldap-wg-maint.yaml":
    ensure  => $ensure,
    content => template('su_ldap/etc/ldapadmin/ldap-wg-maint.yaml.erb'),
  }

  # We want to reload the systemd daemon on any change to the unit
  # file. We use the base::systemd shared library's systemd-daemon-reload
  # to do this.
  file { '/lib/systemd/system/ldap-wg-maint.service':
    ensure  => $ensure,
    content => template('su_ldap/lib/systemd/system/ldap-wg-maint.service'),
    mode    => '0644',
    notify  => Exec['systemd-daemon-reload']
  }

  service { 'ldap-wg-maint':
    ensure  => $service_status,
    require => [
      File['/lib/systemd/system/ldap-wg-maint.service'],
      File["${basedir}/ldap-wg-maint.conf"],
    ],
  }

  ## Cron job
  file { '/etc/cron.d/ldap-wg-maint':
    ensure => $ensure,
    content => template('su_ldap/etc/cron.d/ldap-wg-maint.erb'),
    require => File["${basedir}/ldap-wg-maint.conf"],
  }

}
