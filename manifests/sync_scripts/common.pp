# The common part of the sync_scripts.

class su_ldap::sync_scripts::common (
  Enum['present', 'absent'] $ensure  = 'present',
  String                    $env     = undef,
  String                    $basedir = '/etc/ldapadmin',
) {

  # We need this for systemd-daemon-reload
  include base::systemd

  # We need some definitions for the kerberos ticket service.
  # This is the sync keytab ticket, for service/ldap-$env
  $k5start_service_name = "k5start-ldap-sync-$env"
  $keytab_path          = "${basedir}/ldap-sync-$env.keytab"
  $ticket_file_path     = "/var/run/ldap-sync-$env.tkt"

  # for debugging
  #notify { "k5start_service_name = k5start-ldap-sync-$env ; keytab_path = ${basedir}/ldap-sync-$env.keytab ; ticket_file_path = /var/run/ldap-sync-$env.tkt": }

  # The directory where we put sync script configurations.
  file { $basedir:
    ensure => directory,
  }

  # install the sync scripts package first
  package{ 'libstanford-ldap-sync-scripts-perl':
    ensure => $ensure
  }

  # some of the syncs need this
  ensure_packages(['libcrypt-ssleay-perl'], { ensure => 'present' })

  # Set up a krb5 ticket keep-alive service for the above principal.
  systemd_k5start { $k5start_service_name:
    ensure      => $ensure,
    keytab      => $keytab_path,
    ticket_file => $ticket_file_path,
  }

  if ($ensure == 'present') {
    $service_running = 'running'
    $service_enabled = true
  } else {
    $service_running = 'stopped'
    $service_enabled = false
  }

  service { $k5start_service_name:
    ensure  => $service_running,
    enable  => $service_enabled,
    require => Systemd_K5start[$k5start_service_name],
  }

  # Add the service/ldap-$env keytab. This is single env only, with
  # accountsTreeWrite,peopleTreeWrite,groupsTreeWrite.
  wallet { "service/ldap-$env":
    ensure  => $present,
    path    => $keytab_path,
    require => Base::Wallet["host/$fqdn"],
  }

  # Keytab used to access mailman and Posixgroup.
  wallet { 'service/lists':
    ensure  => $present,
    path    => '/etc/ldap/service-lists.keytab',
    owner   => 'root',
  }

  ### This is not needed to run on the tools hosts. The only host it should 
  ### run against are the masters and replicas, and that's not what this script
  ### does
  ## A small shell script used by the sync scripts. This shell script
  ## detects when the slapd service is running.
  #file { '/usr/bin/checkforslapd':
  #  ensure => $ensure,
  #  source => 'puppet:///modules/su_ldap/usr/bin/checkforslapd',
  #  mode   => '0755',
  #}

}
