# Configure this server to sync the suGAL person attributes using the
# the ldap-sync-sugal-attributes script.
#
# See also https://ikiwiki.stanford.edu/service/ldap/sync-scripts/

class su_ldap::sync_scripts::ldap_sync_sugal_attributes  (
  $ensure           = undef,
  $ldap_master_fqdn = undef,
  $env              = undef,
  String $basedir   = '/etc/ldapadmin',
){

 # Do we want the service to be running or stopped?
  if ($ensure == 'present') {
    $service_status = 'running'
  } elsif ($ensure == 'absent') {
    $service_status = 'stopped'
  } else {
    fail("ensure must be one 'present' or 'absent'")
  }

  # this should be in the syncs scripts libstanford-ldap-sync-scripts-perl'
  ## Load the package containing the ldap-sync-sugal-attributes script.
  #package { 'libstanford-directory-sync-sugal-perl':
  #  ensure  => installed,
  #  require => Package['slapd'],
  #}

  # Install the configuration file for the listener
  file { "${basedir}/ldap-sync-sugal-attributes-daemon.conf":
    ensure  => $ensure,
    content => template('su_ldap/etc/ldapadmin/ldap-sync-sugal-attributes-daemon.conf.erb'),
    require => File[$basedir],
  }

  # Install the configuration file for the NON-listener
  file { "${basedir}/ldap-sync-sugal-attributes.conf":
    ensure  => $ensure,
    content => template('su_ldap/etc/ldapadmin/ldap-sync-sugal-attributes.conf.erb'),
    require => File[$basedir],
  }

  # Install the visibility attributes mapping file.
  file { "${basedir}/sugal-visibility.yaml":
    ensure  => $ensure,
    source  => 'puppet:///modules/su_ldap/etc/ldapadmin/sugal-visibility.yaml',
    require => File[$basedir],
  }

  # Need a cron job to run the sync job against ALL the entries once per
  # day.
  file { '/etc/cron.d/ldap-sync-sugal-attributes':
    ensure  => $ensure,
    mode    => '0644',
    source  => 'puppet:///modules/su_ldap/etc/cron.d/ldap-sync-sugal-attributes',
    require => File["${basedir}/ldap-sync-sugal-attributes.conf"],
  }

  # We want to reload the systemd daemon on any change to the unit
  # file. We use the systemd shared library's systemd-daemon-reload to
  # do this.
  file { '/lib/systemd/system/ldap-sync-sugal-attributes.service':
    ensure  => present,
    content => template('su_ldap/lib/systemd/system/ldap-sync-sugal-attributes.service'),
    mode    => '0644',
    notify  => Exec['systemd-daemon-reload']
  }

  service { 'ldap-sync-sugal-attributes':
    ensure  => $service_status,
    require => [
      File['/lib/systemd/system/ldap-sync-sugal-attributes.service'],
      # Package['libstanford-directory-sync-sugal-perl'],
      Package['libstanford-ldap-sync-scripts-perl'],
    ],
  }

}
