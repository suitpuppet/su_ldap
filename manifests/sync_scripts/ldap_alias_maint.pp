# Support the synchronization process for updating the email aliases
# maintained in the Email Virtual Domain MySQL database with the Directory.
#
# This only runs on master.
#
# A cron job is set up that calls /usr/bin/ldap-alias-maint-remctl which
# is a a thin wrapper around /usr/bin/ldap-alias-maint
#
# See also https://ikiwiki.stanford.edu/service/ldap/sync-scripts/#index6h2

class su_ldap::sync_scripts::ldap_alias_maint (
  $ensure            = undef,
  $env               = undef,
  $ldap_master_fqdn  = undef,
){

  # Do we want the service to be running or stopped?
  if ($ensure == 'present') {
    $service_status = 'running'
  } elsif ($ensure == 'absent') {
    $service_status = 'stopped'
  } else {
    fail("ensure must be one 'present' or 'absent'")
  }

  # Configuration directory
  file { '/etc/ldap-aliases':
    ensure => directory,
    mode   => '0755',
  }

  # The properties file used by /usr/bin/ldap-alias-maint.
  file { '/etc/ldap-aliases/maint.conf':
    ensure  => $ensure,
    content => template('su_ldap/etc/ldap-aliases/ldap-alias-maint.conf.erb'),
    mode    => '0644',
    require => File['/etc/ldap-aliases'],
  }

  # Configure remctl for ldap-aliases.
  file { '/etc/remctl/conf.d/ldap-alias-maint':
    ensure => $ensure,
    source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/ldap-alias-maint',
    mode   => '0644',
  }

  # The MySQL connection informationi and ldap service keytab.
  case $env {
    'prod': {
       wallet { 'idg-ldap-db-ldapaliases':
         ensure => $ensure,
         path   => '/etc/ldap-aliases/mysql.conf',
         type   => 'file'
       }

       wallet { 'service/ldap':
         ensure => $ensure,
         path   => '/etc/ldap-aliases/ldap-service.keytab',
         type   => 'keytab',
       }
    }
    default: {
      wallet { 'idg-ldap-db-ldapaliases-test':
        ensure => $ensure,
        path   => '/etc/ldap-aliases/mysql.conf',
        type   => 'file',
      }

      wallet { "service/ldap":
        ensure => $ensure,
        path   => '/etc/ldap-aliases/ldap-service.keytab',
        type   => 'keytab',
      }
    }
  }

  # The mysql-alias-maint script needs the update.conf file. ???
  file { '/etc/ldap-aliases/update.conf':
    ensure  => $ensure,
    source  => 'puppet:///modules/su_ldap/etc/ldap-aliases/update.conf',
    mode    => '0644',
    require => File['/etc/ldap-aliases'],
  }

  # Set up the cron job. The cron job calls a script provided by the
  # libstanford-ldap-sync-scripts-perl package.
  file { '/etc/cron.d/ldap-alias-maint':
    ensure  => $ensure,
    source  => 'puppet:///modules/su_ldap/etc/cron.d/ldap-alias-maint',
    mode    => '0644',
    require => Package['libstanford-ldap-sync-scripts-perl'],
  }

}
