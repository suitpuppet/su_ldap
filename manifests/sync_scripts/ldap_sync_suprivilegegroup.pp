# The ldap_sync_suprivilegegroup service.
#
# The ldap_sync_suprivilegegroup service copies attributes from one part of the
# directory tree to another. In particular, it copies
# suPrivilegeGroupObject attributes from the people tree to the accounts
# tree.
#
# See also https://ikiwiki.stanford.edu/service/ldap/sync-scripts/

class su_ldap::sync_scripts::ldap_sync_suprivilegegroup  (
  $ensure           = undef,
  $ldap_master_fqdn = undef,
  $env              = undef,
  String $basedir   = '/etc/ldapadmin',
)
{

  # Do we want the service to be running or stopped?
  if ($ensure == 'present') {
    $service_status = 'running'
  } elsif ($ensure == 'absent') {
    $service_status = 'stopped'
  } else {
    fail("ensure must be one 'present' or 'absent'")
  }

  ## Configuration files: one for the cron and one for the listener.
  file {
    "${basedir}/ldap-sync-suprivilegegroup.conf":
      ensure  => $ensure,
      content => template('su_ldap/etc/ldapadmin/ldap-sync-suprivilegegroup.conf.erb'),
      require => File[$basedir];
    "${basedir}/ldap-sync-suprivilegegroup-daemon.conf":
      ensure  => $ensure,
      content => template('su_ldap/etc/ldapadmin/ldap-sync-suprivilegegroup-daemon.conf.erb'),
      require => File[$basedir];
  }

  ## Cron job
  file { '/etc/cron.d/ldap-sync-suprivilegegroup':
    ensure  => $ensure,
    mode    => '0644',
    content => template('su_ldap/etc/cron.d/ldap-sync-suprivilegegroup.erb'),
    require => File["${basedir}/ldap-sync-suprivilegegroup.conf"];
  }

  # We want to reload the systemd daemon on any change to the unit
  # file. We use the base::systemd shared library's systemd-daemon-reload
  # to do this.
  file { '/lib/systemd/system/ldap-sync-suprivilegegroup.service':
    ensure  => $ensure,
    content => template('su_ldap/lib/systemd/system/ldap-sync-suprivilegegroup.service'),
    mode    => '0644',
    notify  => Exec['systemd-daemon-reload'],
  }

  service { 'ldap-sync-suprivilegegroup':
    ensure  => $service_status,
    require => [
      File['/lib/systemd/system/ldap-sync-suprivilegegroup.service'],
      File["${basedir}/ldap-sync-suprivilegegroup-daemon.conf"],
    ],
  }


}
