#
# Set up a cron job to duplicate entries from one tree to another using
# the /usr/bin/ldap-sync-tree script.
#
# See also https://ikiwiki.stanford.edu/service/ldap/sync-scripts/

define su_ldap::sync_scripts::posix_account_sync (
  $ensure         = undef,
  $ldap_master_fqdn = undef,
  $env            = undef,
  String $basedir = '/etc/ldapadmin',
  $workgroup_stem = 'NONE',
  $cron_minute    = '32',
  $cron_hour      = '*',
  $cron_day       = '*'
) {

  # Nice default for the stem
  case $workgroup_stem {
    'NONE':  { $stem = $name           }
    default: { $stem = $workgroup_stem }
  }

  case $ensure {
    'absent': {
      file { "${basedir}/posixAccount-sync-${stem}.conf":
        ensure => absent,
      }
      file { "/etc/cron.d/posixAccount-sync-${stem}":
         ensure => absent,
      }
    }
    default: {
      if ($stem == 'NONE') { fail('WorkGroup stem not set') }
      # transistion - remove after first use
      file {
        "${basedir}/posixAccount-sync-${stem}.conf":
          content => template('su_ldap/etc/ldapadmin/posixAccount-sync.conf.erb'),
          mode    => '0755';
        "/etc/cron.d/posixAccount-sync-${stem}":
          content => template('su_ldap/etc/cron.d/posixAccount-sync.erb'),
          mode    => '0755';
      }
    }
  }
}
