# This class manages the configuration file used by the
# /usr/bin/ldap-mail-lists script. From the man page for ldap-mail-lists:
# "This script adds, updates, or deletes mail lists maintained in the LDAP
# directory.  The membership is driven by the members defined in mailman"

class su_ldap::ldapadmin::mail_lists(
  Enum['present', 'absent'] $ensure,
  Optional[String]          $master = undef,
) {

  if ($ensure == 'present' and (! $master)) {
    fail("missing master parameter")
  }

  file { '/etc/ldapadmin/ldap-mail-lists.conf':
    ensure  => $ensure,
    mode    => '0644',
    content => template('su_ldap/etc/ldapadmin/ldap-mail-lists.conf.erb'),
    require => File['/etc/ldapadmin'],
  }

  # Keytab used to access mailman. Don't remove in case one of the
  # sync-script classes needs it.
  if ($ensure == 'present') {
    base::wallet { 'service/lists':
      ensure  => 'present',
      path    => '/etc/ldap/service-lists.keytab',
      owner   => 'root',
      require => File['/etc/ldapadmin'],
    }
  }

  # Install the remctl interface
  file { '/etc/remctl/conf.d/ldaputil-mail-lists':
    ensure => $ensure,
    source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/ldaputil-mail-lists',
    mode   => '0644',
  }

}
