# This class manages the configuration for the script
# /usr/bin/ldap-watcher. From the script's man page: "Watch changes
# published in OpenLDAP's accesslog".

class su_ldap::ldapadmin::ldap_watcher(
  Enum['present', 'absent'] $ensure,
) {

  file { '/etc/ldapadmin/ldap-watcher.conf':
    ensure => $ensure,
    mode    => '0644',
    content => template('su_ldap/etc/ldapadmin/ldap-watcher.conf.erb'),
    require => File['/etc/ldapadmin'],
  }


}
