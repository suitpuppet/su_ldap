# This class manages the configuration for the script
# /usr/bin/ldap-log-store. From the script's man page: "Read an OpenLDAP
# log and summarize the queries and store the summaries in a MySQL
# database."

class su_ldap::ldapadmin::log_store(
  Enum['present', 'absent'] $ensure,
  Optional[String]          $db_name,
) {

  if ($ensure == 'present' and (! $db_name)) {
    fail("missing db_name parameter")
  }

  file { '/etc/ldapadmin/ldap-log-store.conf':
    ensure => $ensure,
    mode    => '0644',
    content => template('su_ldap/etc/ldapadmin/ldap-log-store.conf.erb'),
    require => File['/etc/ldapadmin'],
  }

}
