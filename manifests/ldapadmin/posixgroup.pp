# This class manages the configuration for the script
# /usr/bin/ldap-posix-group. From the script's man page: "This is a remctl
# wrapper script that invokes ldap-group-maint and restricts the options
# to setting up or showing a posix group.

class su_ldap::ldapadmin::posixgroup(
  Enum['present', 'absent'] $ensure,
  $env    = undef,
  String $basedir = '/etc/ldapadmin',
) {

  # third, pick your bucket and credentials
  case $env {
    'prod' : {
      $ldap_master_fqdn = 'ldap0.stanford.edu'
      $lsdb_host        = 'lsdb.stanford.edu'
      $generate_gid     = '0'
    }
    default: {
      $ldap_master_fqdn = "ldap-${env}0.stanford.edu"
      $lsdb_host        = 'lsdb-$env.stanford.edu'
      $generate_gid     = '1'
    }
  }

  ## moved to the sync scipts module that installs libstanford-ldap-sync-scripts-perl
  ## Configuration for Remctl posixgroup
  #file { '/etc/ldapadmin/ldap-group-maint.conf':
  #  ensure  => $ensure,
  #  mode    => '0644',
  #  content => template('su_ldap/etc/ldapadmin/ldap-group-maint.conf.erb'),
  #  require => File['/etc/ldapadmin'],
  #}

  # Keytab used to access mailman and Posixgroup.
  if ($ensure == 'present') {
    wallet { 'service/lists':
      ensure  => 'present',
      path    => '/etc/ldap/service-lists.keytab',
      owner   => 'root',
      require => File[$basedir],
    }
  }

  # Install the remctl interface
  file { '/etc/remctl/conf.d/posixgroup':
    ensure => $ensure,
    source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/posixgroup',
    mode   => '0644',
  }

}
