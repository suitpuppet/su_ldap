# This class manages the configuration used by the /usr/bin/ldap-krb-audit
# script. From the man page of /usr/bin/ldap-krb-audit" "This scripts
# performs scans of OpenLDAP directory ACLs, the supporting Kerberos
# entries, and cn=groups entries.". For more details, see the entire man
# page.

class su_ldap::ldapadmin::krb_audit(
  Enum['present', 'absent'] $ensure,
) {

  file { '/etc/ldapadmin/ldap-krb-audit.template':
    ensure  => $ensure,
    mode    => '0644',
    source  => 'puppet:///modules/su_ldap/etc/ldapadmin/ldap-krb-audit.template',
    require => File['/etc/ldapadmin'],
  }

}
