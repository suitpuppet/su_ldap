# This class manages the configuration file used by the
# /usr/bin/ldap-mail-destinations. From the man page for
# ldap-mail-destinations: "This script reads the suMailDrop attribute for
# all entries in the accounts tree, summaries entries by destination
# domain, and stores the results in a MySQL database."

class su_ldap::ldapadmin::mail_destinations(
  Enum['present', 'absent'] $ensure,
) {

  file { '/etc/ldapadmin/ldap-mail-destinations.conf':
    ensure  => $ensure,
    mode    => '0644',
    source  => 'puppet:///modules/su_ldap/etc/ldapadmin/ldap-mail-destinations.conf',
    require => File['/etc/ldapadmin'],
  }
}
