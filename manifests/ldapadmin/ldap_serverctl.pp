# This class manages the configuration for /usr/bin/ldap-serverctl. From
# the man page of ldap-serverctl: "A simple script to run puppet or
# restart slapd ldap servers. Supply an action and the directory
# environment and the script will use remctl to update the directory
# servers in that environment. The scripts updates either master or
# replicas or both in a directory environment. The default is to update
# replicas."

class su_ldap::ldapadmin::ldap_serverctl(
  Enum['present', 'absent'] $ensure,
) {

  file { '/etc/ldapadmin/server-list':
    ensure  => $ensure,
    mode    => '0644',
    source  => 'puppet:///modules/s_ldap/etc/ldapadmin/server-list',
    require => File['/etc/ldapadmin'],
  }

}
