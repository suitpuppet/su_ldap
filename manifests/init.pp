#
#
# $enable_lb_bigip: enable the F5 load-balancer control interface. Usually only on
# replicas.
#
# $enable_lbcd: enable LBCD, that is, the LBDNS performance polling client. Usually only on
# replicas.
#
##################################################################################
#
# Possible parameters:
#
#  - which flavor of OpenLDAP package to install
#  - install sync scripts
#  - authentication methods
#  - support whois
#  - hosting model
#  - where do we store the LDAP databases and log files
#  - do we enable bundle remctl service?
#
# Another consideration is where we build the ldap-tools servers from.
# They take only the ldap-utils, libldap, libnet-ldap-perl,
# libnet-ldapapi-perl, libstanford-ldapadmin-perl, libstanford-ldapserver-perl,
# python-ldap, the passive monitoring script(s) and the sync scripts

# $distribution: A valid Debian distribution. See the file apt_setup for
#   more information and examples.
#
# $repository: If the package is found in a non-standard location,
#   you can indicate the repository here. See the file apt_setup for
#   more information and examples.

# We default to enabling both connection methods (ldap:/// and ldaps:///)
# for all servers regardless of which authentication method(s)
# enabled. HOWEVER, ldap:/// is not safe for simple bind servers, so if
# both ldap:/// and simple bind authentication are enabled it is
# IMPERATIVE that you put firewall restrictions on port 389 so that only
# monitoring or special-purpose servers get access to that port. This must
# be done in the calling class.

class su_ldap (
  $hosting_model = 'traditional',
  #
  $env            = undef,
  #
  $auth_gssapi    = true,
  $auth_simple    = true,
  #
  $ldap_over_tcp  = true,  # enable ldap:///
  $ldap_over_tls  = true,  # enable ldaps:///
  #
  $ldap_debian_distribution = 'stretch',
  $ldap_debian_archive      = 'debian-stanford',
  $ldap_debian_repository   = undef,
  $sasl_debian_distribution = 'stretch',
  $sasl_debian_archive      = 'debian-stanford',
  $sasl_debian_repository   = undef,
  #
  $keytab_path = '/etc/krb5.keytab',
  #
  $port_389_cidrs = [],
  $port_636_cidrs = [],
  #
  Boolean $enable_whois        = false,
  #
  Boolean $enable_lb_bigip     = false,
  Boolean $enable_lbcd         = false,
){

  ## ERROR CHECKING ##
  if !($hosting_model in [ 'traditional', 'container', 'tools' ]) {
      fail("Unknown hosting model ${hosting_model}")
  }

  # Install apt files so we load the correct version of OpenLDAP.
  class { 'su_ldap::apt_setup':
    ldap_debian_distribution => $ldap_debian_distribution,
    ldap_debian_archive      => $ldap_debian_archive,
    ldap_debian_repository   => $ldap_debian_repository,
    sasl_debian_distribution => $sasl_debian_distribution,
    sasl_debian_archive      => $sasl_debian_archive,
    sasl_debian_repository   => $sasl_debian_repository,
  }

  ## PACKAGE SETUP ##
  include su_ldap::packages


  ## Basic configuration: /etc/ldap/ldap.conf, /etc/default/slapd, et al.
  ## This is all managed in the su_ldap::config class.

  # Make sure /etc/ldap exists and is a directory.
  file { '/etc/ldap':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  class { 'su_ldap::config':
    hosting_model => $hosting_model,
    keytab_path   => $keytab_path,
    auth_gssapi   => $auth_gssapi,
    auth_simple   => $auth_simple,
    ldap_over_tcp => $ldap_over_tcp,
    ldap_over_tls => $ldap_over_tls,
  }

  ## We don't install the sync scripts in this class. If they are needed,
  ## use the su_ldap::sync_scripts class.

  ## We don't install the certificate in this class. Instead, call the
  ## su_ldap::certificate from the class that call 'su_ldap'.

  ## Authentication methods (simple bind and GSSAPI)
  class { 'su_ldap::authentication':
    auth_gssapi => $auth_gssapi,
    auth_simple => $auth_simple,
  }

  if ($hosting_model == 'traditional') {
    class { 'su_ldap::traditional':
      keytab_path    => $keytab_path,
      port_389_cidrs => $port_389_cidrs,
      port_636_cidrs => $port_636_cidrs,
    }
  }

  ## Various optional services
  if ($enable_whois) {
    class { 'su_ldap::config::whoisd':
      ensure => present
    }
  } else {
    class { 'su_ldap::config::whoisd':
      ensure => absent
    }
  }

  # Hardware (F5) load-balancing API support.
  if ($enable_lb_bigip) {
     class { 'su_ldap::lb_bigip':
       env    => $env,
       ensure => present,
     }
  } else {
    class { 'su_ldap::lb_bigip':
      env    => $env,
      ensure => absent,
    }
    ### already declared for both present and absent in class { 'su_ldap::lb_bigip':
    #file { '/etc/bigip/bigip.conf':
    #  ensure => absent,
    #}
  }

  # LBDNS (software load-balancing)
  if ($enable_lbcd) {
    class { 'su_ldap::lbcd':
      ensure => 'present',
    }
  } else {
    class { 'su_ldap::lbcd':
      ensure => 'absent',
    }
  }

}
