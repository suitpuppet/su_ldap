class su_ldap::traditional (
  $keytab_path    = '/etc/krb5.keytab',
  #
  $port_389_cidrs   = [],
  $port_636_cidrs   = [],
  #
  Enum['present', 'absent'] $ensure = 'absent',
) {

  ## Firewall rules
  class { 'su_ldap::traditional::firewall':
    port_389_cidrs => $port_389_cidrs,
    port_636_cidrs => $port_636_cidrs,
  }

  ## Keytabs
  # Make sure the keytab for the ldap/hostname service principal is installed.
  # It is not meant to be primary.
  #
  # Note that we require that Base::Wallet["host/${::fqdn}"] already be
  # installed.
  base::wallet { "ldap/${::fqdn}":
    ensure  => present,
    path    => $keytab_path,
    primary => false,
    require => Base::Wallet["host/${::fqdn}"],
  }

  # A keytab used by remctl commands to access the directory on the
  # localhost host. In particular, used by the script that the "ldaputil"
  # remctl interface calls.
  base::wallet { "service/${::hostname}":
    ensure  => present,
    path    => '/etc/ldap/ldap-localhost.keytab',
    type    => 'keytab',
    require => File['/etc/ldap'],
  }

  ## Ensure slapd service running

  ## Remctl
  include su_ldap::traditional::remctl

  ## krb5 ticket cache

  ## sysctl settings(?)

  ## ldap-reports

  ## Sync config to a git repo (ldap_tools::git_config)
  #if ($git_config) {
  #  include su_ldap::config::git_config
  #}

  ## AWS S3 backup

  ## Non-sync cron jobs

  ## Load balancing for traditional replicas (including LBDNS)

}
