# IMPORTANT NOTE: This class does not install the
# private-key/certificate. You must install it in the calling class.

# $auth_gssapi: if true support GSSAPI authentication.

# $auth_simple: if true support simple-bind authentication.

# The default is to install both but to then control simple binds by
# two methods: iptables access to 636, and whether the saslauthd
# service is actually running

class su_ldap::authentication (
  $auth_gssapi = true,
  $auth_simple = true,
) {

  if ((!$auth_gssapi) and (!$auth_simple)) {
    crit("at least one of auth_gssapi and/or auth_simple must be enabled")
  }

  # Configure SASL for slapd.
  file { '/etc/ldap/sasl2/slapd.conf':
    content => template('su_ldap/etc/ldap/sasl2/slapd.conf.erb'),
    owner   => root,
    group   => root,
    mode    => '0644',
  }

  # saslauthd package
  package { 'sasl2-bin': ensure => installed }

  # Make sure the saslauthd service, the service that allows "simple"
  # binds to work, is running.
  # TODO: see if we can specify this at run time, especially for containers
  if ($auth_simple) {
    $saslauthd_service_state = 'running'
  } else {
    $saslauthd_service_state = 'stopped'
  }

  file { '/etc/default/saslauthd':
    ensure  => present,
    source  => 'puppet:///modules/s_ldap/etc/default/saslauthd',
    require => Package['sasl2-bin'];
  }

  service { 'saslauthd':
    ensure    => $saslauthd_service_state,
    require   => Package['sasl2-bin'],
    hasstatus => false,
    status    => 'test -f /etc/nosaslauthd || pidof saslauthd',
  }

  ## Do the iptables dance elsewhere, because it's different depending on what
  ## type of server or location is used, and this module is generic for building
  ## the auth part
}
