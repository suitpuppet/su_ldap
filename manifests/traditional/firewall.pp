class su_ldap::traditional::firewall (
  $port_389_cidrs = [],
  $port_636_cidrs = [],
) {

  # LDAP
  base::iptables::rule {'ldap':
    ensure      => 'present',
    description => 'Allow port 389 ldap:// connections',
    source      => $port_389_cidrs,
    port        => ['389'],
    protocol    => 'tcp';
  }

  # LDAPS
  base::iptables::rule {'ldaps':
    ensure      => 'present',
    description => 'Allow port 636 ldap:// connections',
    source      => $port_636_cidrs,
    port        => ['636'],
    protocol    => 'tcp';
  }

}
