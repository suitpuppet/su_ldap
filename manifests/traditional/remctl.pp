class su_ldap::traditional::remctl (
) {

  # Install remctl configurations
  file {
    '/etc/remctl/conf.d/directory-base':
      mode   => '0644',
      source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/directory-base';
    '/etc/remctl/conf.d/ldaputil':
      mode   => '0644',
      source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/ldaputil';
    '/etc/remctl/acl/its-rc-posix-gm':
      mode   => '0644',
      source => 'puppet:///modules/su_ldap/etc/remctl/acl/its-rc-posix-gm';
    '/etc/remctl/acl/tools':
      mode   => '0644',
      source => 'puppet:///modules/su_ldap/etc/remctl/acl/tools';
#    '/etc/remctl/conf.d/posixgroup':
#      mode   => '0644',
#      source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/posixgroup';
#    '/etc/remctl/acl/ldap':
#      mode   => '0644',
#      source => 'puppet:///modules/s_ldap/etc/remctl/acl/ldap';
#    '/etc/remctl/conf.d/ldapcontrol':
#      mode   => '0644',
#      source => 'puppet:///modules/s_ldap/etc/remctl/conf.d/ldapcontrol';
#    '/etc/remctl/conf.d/ldapsync':
#      mode   => '0644',
#      source => 'puppet:///modules/s_ldap/etc/remctl/conf.d/ldapsync';
  }

}
