class su_ldap::certificate (
  $load_balanced_name = undef,
) {

  ## ERROR CHECKING ##
  if (!$load_balanced_name) {
      fail("Missing required parameter 'load_balanced_name'")
  }

  # Install a certificate that has as its CN (common name) the
  # load-balanced name and as subject-alternative names the name of the
  # master and all the replicas. For example, for the dev environment the
  # common name should be "ldap-dev.stanford.edu" and the SANs should be:
  #
  #   ldap-dev0.stanford.edu
  #   ldap-dev1.stanford.edu
  #   ldap-dev2.stanford.edu
  #   ldap-dev3.stanford.edu
  #   ldap-dev4.stanford.edu
  #   ldap-dev5.stanford.edu
  #   ldap-dev6.stanford.edu
  #
  # Always have at least 5 (two masters and three replicas); it wouldn't
  # hurt to throw in a few extras in case you need to add another server
  # later.
  #
  # Save the certificate in the cert-files module with the load-balanced
  # name, e.g., "ldap-dev.stanford.edu". Use the load-balanced name even
  # if the environment is not load-balanced.
  apache::cert::comodo{$load_balanced_name:
    ensure   => present,
    identity => $load_balanced_name,
    keyname  => "ssl-key/${load_balanced_name}",
  }

  # We also need to install a file containing all the InCommon
  # intermediate and root certificates that make up the certificate chain
  # for the above certificate. If we don't do this, certain clients, when
  # trying to STARTTLS, will be unhappy.
  file { '/etc/ssl/certs/ldap-cabundle.pem':
    ensure => present,
    source => 'puppet:///modules/su_ldap/etc/ssl/certs/ldap-cabundle.pem',
  }


}
