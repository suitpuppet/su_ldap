class su_ldap::bundle (
  Enum['present', 'absent'] $ensure,
  Optional[String]          $env,
) {

  file { '/etc/ldap-bundle-maint.conf':
    ensure  => $ensure,
    mode    => '0644',
    content => template("su_ldap/etc/ldap-bundle-maint.conf.erb"),
  }

}
