class su_ldap::config::logging(
  $ldap_log_dir         = '/var/lib/ldap/logs',
  $rotate_logs_into_afs = false,
) {

  ## LDAP LOGS
  # We put the logs in a special place.
  file { $ldap_log_dir:
    ensure  => directory,
    require => File['/var/lib/ldap'],
  }

  file { "${ldap_log_dir}/OLD":
    ensure  => directory,
    require => File[$ldap_log_dir],
  }

  # Make a soft link to this ldap log file as well as to the OLD ldap directory.
  file {
    '/var/log/ldap':
      ensure => link,
      target => "${ldap_log_dir}/ldap";
    '/var/log/OLD-ldap':
      ensure  => link,
      target  => "${ldap_log_dir}/OLD",
      require => File["${ldap_log_dir}/OLD"];
  }

  # The LDAP software sends logs to the local4 facility of syslog. We want
  # to use rsyslog to send these local4 syslogs to a file. We assume that
  # the package rsyslog is already installed by another class.
  # ##require => Package['rsyslog'],
  file { '/etc/rsyslog.d/50-ldap-remote.conf':
    ensure  => present,
    content => template('su_ldap/etc/rsyslog.d/50-ldap-remote.conf.erb'),
  }

  ## ROTATING LOGS (NEWSYSLOG)

  # A bit nonstandard but used for additional newsyslog rules called
  # as part of cron jobs
  file { '/etc/newsyslog': ensure => directory }

  # The files rotated locally are rotated into $ldap_log_dir/OLD.  We only
  # rotate logs into AFS for production servers: the ldap.erb template
  # will include the configuration to do rotation to AFS based on the
  # value of the $rotate_logs_into_afs parameter.
  file { '/etc/newsyslog.daily/ldap':
    mode    => '0644',
    content => template('su_ldap/etc/newsyslog.daily/ldap.erb'),
    require => Package['newsyslog'];
  }

  # Backup cn=config into AFS.
  file { '/etc/newsyslog.daily/ldap-cn-config':
    mode    => '0644',
    source  => 'puppet:///modules/su_ldap/etc/newsyslog.daily/ldap-cn-config',
    require => Package['newsyslog'];
  }

  ## FILTER-SYSLOG
  # Filter known trivial/non-critical messages out of the logs
  file { '/etc/filter-syslog/ldap':
    mode   => '0644',
    owner  => root,
    group  => root,
    source => 'puppet:///modules/su_ldap/etc/filter-syslog/ldap',
  }

  ## FILTER-SYSLOG
  # Filter known trivial/non-critical messages out of the logs
  file { '/etc/filter-syslog/ldap-cn-config':
    mode   => '0644',
    owner  => root,
    group  => root,
    source => 'puppet:///modules/su_ldap/etc/filter-syslog/ldap-cn-config',
  }


}
