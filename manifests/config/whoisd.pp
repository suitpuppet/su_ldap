##############################################################################
# Definition for configuring the whois and finger service
##############################################################################

class su_ldap::config::whoisd(
  $ensure = 'present'
) {

  case $ensure {

    #### ABSENT ####
    'absent': {
      file {
        '/etc/remctl/conf.d/whoisd':       ensure => absent;
        '/etc/remctl/conf.d/whois-search': ensure => absent;
        '/etc/remctl/acl/whois-search':    ensure => absent;
        '/etc/whoisd/whoisd.conf':         ensure => absent;
        '/etc/whoisd/whoisd-remctl.conf':  ensure => absent;
      }
      package {'stanford-whoisd': ensure => absent }
    }

    #### PRESENT ####
    'present': {
      # principal that is used to query the directory
      base::wallet { 'service/whois':
        path    => '/etc/whoisd/keytab',
        owner   => 'root',
        ensure  => present,
        require => File['/etc/whoisd'];
      }

      # Ports listened to by whois and finger
      base::iptables::rule { 'whois':
        ensure      => 'present',
        description => 'Allow finger and whois connections',
        port        => ['43','79'],
        protocol    => 'tcp',
      }

      # Intall the package after the configuration is in place
      package {'stanford-whoisd':
        ensure => installed,
        require => File['/etc/whoisd/whoisd.conf'];
      }

      # Make sure whoisd and fingerd are running.

      ## WHOIS
      # We want to reload the systemd daemon on any change to the unit
      # file. We use the systemd shared library's systemd-daemon-reload to
      # do this.
      file { '/lib/systemd/system/whoisd.service':
        ensure => present,
        source => 'puppet:///modules/su_ldap/lib/systemd/system/whoisd.service',
        mode   => '0644',
        notify => Exec['whoisd-systemd-daemon-reload']
      }

      service { 'whoisd':
        ensure  => 'running',
        require => [
          File['/lib/systemd/system/whoisd.service'],
          File['/etc/whoisd/whoisd.conf'],
          Package['stanford-whoisd'],
        ],
      }

      ## FINGER
      file { '/lib/systemd/system/fingerd.service':
        ensure => present,
        source => 'puppet:///modules/su_ldap/lib/systemd/system/fingerd.service',
        mode   => '0644',
        notify => Exec['whoisd-systemd-daemon-reload']
      }

      service { 'fingerd':
        ensure  => 'running',
        require => [
          File['/etc/whoisd/fingerd.conf'],
        ],
      }

      exec { 'whoisd-systemd-daemon-reload':
        path        => '/bin:/usr/bin:/sbin:/usr/sbin',
        command     => 'systemctl daemon-reload',
        refreshonly => true,
      }

      file {
        '/etc/whoisd':
          mode    => '0755',
          ensure  => directory;
        '/etc/whoisd/fingerd.conf':
          mode    => '0644',
          content => template('su_ldap/etc/whoisd/fingerd.conf.erb'),
          require => File['/etc/whoisd'];
        '/etc/whoisd/whoisd.conf':
          mode    => '0644',
          content => template('su_ldap/etc/whoisd/whoisd.conf.erb'),
          require => File['/etc/whoisd'];
        '/etc/whoisd/whoisd-remctl.conf':
          mode    => '0644',
          content => template('su_ldap/etc/whoisd/whoisd-remctl.conf.erb'),
          require => File['/etc/whoisd'];
        '/etc/remctl/conf.d/whoisd':
          mode   => '0644',
          source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/whoisd';
      }
    }
  }
}



