class su_ldap::config::ldap_alias_maint (
  $ensure       = 'present',
  $env          = undef,
){

  if (!$env) {
    fail("missing required parameter 'env'")
  }

  # Configuration directory
  file { '/etc/ldap-aliases':
    ensure => directory,
    mode   => '0755',
  }

  # Add the service/ldap keytab to the above primary keytab. This is only
  # needed by the master.
  if ( $env == 'prod') {
    $service_name = "service/ldap"
    $master_name = "ldap/ldap-master.stanford.edu"
  } else {
    $service_name = "service/ldap-${env}"
    $master_name = "ldap/ldap-${env}master.stanford.edu"
  }

  base::wallet { $service_name:
    ensure  => present,
    path    => '/etc/krb5.keytab',
    primary => false,
    require => Base::Wallet["host/${::fqdn}"],
  }

  # Note: ldap/ldap-${env}master needs to come after service/ldap-env
  base::wallet { $master_name:
    ensure  => present,
    path    => '/etc/krb5.keytab',
    primary => false,
    type    => 'keytab',
    require => Base::Wallet["host/${::fqdn}"],
  }

  # The properties file used by /usr/bin/ldap-alias-maint.
  file { '/etc/ldap-aliases/maint.conf':
    content => template('su_ldap/etc/ldap-aliases/ldap-alias-maint.conf.erb'),
    mode    => '0644',
    require => File['/etc/ldap-aliases'],
  }

  # Configure remctl for ldap-aliases.
  file { '/etc/remctl/conf.d/ldap-alias-maint':
    source => 'puppet:///modules/su_ldap/etc/remctl/conf.d/ldap-alias-maint',
    mode   => '0644',
  }

  # The MySQL connection information.
  if ($env == 'prod') {
    base::wallet { 'idg-ldap-db-ldapaliases':
      path => '/etc/ldap-aliases/mysql.conf',
      type => 'file'
    }
  } else {
    base::wallet { 'idg-ldap-db-ldapaliases-test':
      path => '/etc/ldap-aliases/mysql.conf',
      type => 'file',
    }
  }

  # The mysql-alias-maint script needs the update.conf file. ???
  file { '/etc/ldap-aliases/update.conf':
    source  => 'puppet:///modules/su_ldap/etc/ldap-aliases/update.conf',
    mode    => '0644',
    require => File['/etc/ldap-aliases'],
  }


}
