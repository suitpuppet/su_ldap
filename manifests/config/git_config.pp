class su_ldap::config::git_config (
  $ensure       = 'present',
  $env          = undef,
  $ldap_role    = undef,
  $ssh_key      = '/etc/ldap-git-config/git-cn-config-sshkey.private',
){

  if ($env == 'prod') {
    $suffix = 'prod'
  } else {
    $suffix = 'nonprod'
  }

  if (!$ldap_role) {
    fail("missing required parameter 'ldap_role'")
  }

  if (!$env) {
    fail("missing required parameter 'env'")
  }

  if (!$ssh_key) {
    fail("missing required parameter 'ssh_key'")
  }
 
  # Setup GIT URL
  $git_url = "git@code.stanford.edu:authnz/ldap-${ldap_role}-config/${env}.git"

  ## 1. Setup the configuration directory
  if ($ensure == 'present') {
    file { '/etc/ldap-git-config':
      ensure => directory,
    }
  } else {
    file { '/etc/ldap-git-config':
      ensure => absent,
    }
  }

  ## 2. Create configuration file
  file { '/etc/ldap-git-config/config':
    ensure  => $ensure,
    content => template('su_ldap/etc/ldap-git-config/config.erb'),
    require => File['/etc/ldap-git-config'],
  }

  ## 3. Install Keys
  # Get the ssh private key needed to read/write to the
  # Git repository.
  $wallet_name = "service/its-idg/git-cn-config-sshkey/ldap-${suffix}"
  base::wallet { $wallet_name:
    ensure  => $ensure,
    path    => '/etc/ldap-git-config/git-cn-config-sshkey.private',
    type    => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => File['/etc/ldap-git-config'],
  }

  # Install the public key as well.
  file { '/etc/ldap-git-config/git-cn-config-sshkey.public':
    ensure => $ensure,
    source => "puppet:///modules/su_ldap/etc/ldap-git-config/git-cn-config-sshkey-${suffix}.public",
    require => File['/etc/ldap-git-config'],
  }

  ## 4. Install the necessary package: Uninstall to transition to gomplate
  package { 'stanford-ldap-git-config':
    ensure => absent
  }

  ## 5. Use a Puppet-managed known_hosts file. This file will need to be
  ##    regenerated whenever code.stanford.edu changes its ssh-key.
  file { '/root/.ssh/known_hosts':
    ensure => $ensure,
    source => 'puppet:///modules/su_ldap/root/dot_ssh/known_hosts',
  }

  ## Command to Run: uninstall to use cn-config-to-git.sh
  file { '/root/ldap-to-git.sh':
    ensure  => absent,
  }

  file { '/root/cn-config-to-git.sh':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source => 'puppet:///modules/su_ldap/root/cn-config-to-git.sh',
  }

}

