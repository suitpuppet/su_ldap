# We use Puppet "stages" so that this class will be called _after_
# apt_setup.

class su_ldap::packages {

  # Install the "slapd" package.
  package { 'slapd':
    ensure  => installed,
    require => File['/etc/default/slapd'],
  }

  # Install a bunch of other packages that all depend on "slapd".
  #
  # Note that the debug package naming changed with Debian stretch.
  # Since this Puppet model is intended for stretch and beyond
  # we do not have to worry about previous versions of Debian.
  package{
#    'libldap-2.4-2-dbgsym':
#      ensure  => installed,
#      require => Package['slapd'];
    'libsasl2-modules-gssapi-mit':
      ensure  => installed;
    'ldap-utils':
      ensure  => installed,
      require => Package['slapd'];
    'libnet-ldap-perl':
      ensure  => installed,
      require => Package['slapd'];
    'libnet-ldapapi-perl':
      ensure  => installed,
      require => Package['slapd'];
  }

  # Stanford-specific packages
  package{
    'stanford-server-directory':
      ensure  => installed,
      require => Package['slapd'];
  }

  # Install the package containing mdb_copy
  package { 'lmdb-utils':
    ensure => installed
  }

}
