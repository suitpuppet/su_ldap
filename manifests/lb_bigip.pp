# Class to install required keytabs and lb::bigip module

class su_ldap::lb_bigip (
 $env  = undef,
 $ensure = 'present',
){

  if $ensure == 'present' {
  
    if $env == 'prod' {
      # Hardware load balancer support
      base::wallet { "ldap/ldap.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["ldap/$fqdn"],
        ensure  => present,
      }
      base::wallet { "ldap/ldap-lb.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
      }
      base::wallet { "ldap/ldap-app.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
      }
      base::wallet { "host/ldap.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
      }
      base::wallet { "host/ldap-lb.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
      }
      base::wallet { "host/ldap-app.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
      }
    } else {
      # Hardware load balancer support
      base::wallet { "ldap/ldap-${env}.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["ldap/$fqdn"],
        ensure  => present,
      }
      base::wallet { "ldap/ldap-${env}-lb.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
      }
      base::wallet { "host/ldap-${env}.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
      }
      base::wallet { "host/ldap-${env}-lb.stanford.edu":
        path    => '/etc/krb5.keytab',
        primary => false,
        require => Base::Wallet["host/$fqdn"],
        ensure  => present,
       }
    } # end if prod else

    lb::bigip { 'ldap':
      ensure        => $ensure,
      bigip_service => "ldap-${env}",
      remctl_cmd    => 'ldap-bigip',
      signal_port   => '8389',
    }
    file { '/etc/bigip/bigip.conf':
      ensure => $ensure,
      mode   => '0664',
      source => 'puppet:///modules/su_ldap/etc/bigip/bigip.conf',
    }
  } else { 

    lb::bigip { 'ldap':
      ensure        => $ensure,
      bigip_service => "ldap-${env}",
      remctl_cmd    => 'ldap-bigip',
      signal_port   => '8389',
    }
    file { '/etc/bigip/bigip.conf':
      ensure => $ensure,
      mode   => '0664',
      source => 'puppet:///modules/su_ldap/etc/bigip/bigip.conf',
    }

  }  # end if present else

}
