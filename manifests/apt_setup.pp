# Set up apt to get the correct Debian packages.

# TO DO: migrate from su_apt::source to apt::source.

# $ldap_distribution: A valid Debian distribution to be used for OpenLDAP
#   packages. This can be the usual ones, such as "jessie", "stretch",
#   "buster", "sid".  It can also be one of the stanford special ones,
#   e.g., "stretch-openldap-dev", "stretch-openldap-prod", etc.
#
# $sasl_distribution: A valid Debian distribution to be used for Cyrus
#   SASL packages.
#
#
#  #########
#  Example 1
#
#  class {'su_ldap::apt_setup':
#    distribution => 'stretch'
#  }
#
#  This will install the stock OpenLDAP packages from the stretch distribution.
#
#
#  #########
#  Example 2:
#
#  class {'su_ldap::apt_setup':
#    distribution => 'stretch-openldap-dev'
#  }
#
# This will install the OpenLDAP packages from the special stretch-acs-dev
# distribution that resides in the "stanford" repository.


class su_ldap::apt_setup (
  $ldap_debian_distribution = undef,
  $ldap_debian_archive      = 'debian-stanford',
  $ldap_debian_repository   = undef,
  #
  $sasl_debian_distribution = undef,
  $sasl_debian_archive      = 'debian-stanford',
  $sasl_debian_repository   = undef,
) {


  $cur_codename = $facts['os']['distro']['codename']

  if (($ldap_debian_distribution != undef) and ($ldap_debian_distribution == $cur_codename)) {
    $ldap_distro = undef
  } else {
    $ldap_distro = $ldap_debian_distribution
  }

  if (($sasl_debian_distribution != undef) and ($sasl_debian_distribution == $cur_codename)) {
    $sasl_distro = undef
  } else {
    $sasl_distro = $sasl_debian_distribution
  }


  # Only need to do something if $debian_distribution is defined.

  if      (($ldap_distro == undef) and ($sasl_distro == undef)) {
    # BOTH undefined, so do nothing.
  } elsif (($ldap_distro == undef) and ($sasl_distro != undef)) {
    # Only SASL defined
    su_apt::source { $sasl_distro:
      comment       => 'Special sources.list.d file for Cyrus SASL',
      archive       => $sasl_debian_archive,
      distributions => [$sasl_distro],
      repository    => $sasl_debian_repository,
      notify        => Exec['ldap_aptitude_update'];
    }
  } elsif (($ldap_distro != undef) and ($sasl_distro == undef)) {
    # Only LDAP defined
    su_apt::source { $ldap_distro:
      comment       => 'Special sources.list.d file for OpenLDAP',
      archive       => $ldap_debian_archive,
      distributions => [$ldap_distro],
      repository    => $ldap_debian_repository,
      notify        => Exec['ldap_aptitude_update'];
    }
  } elsif (($ldap_distro != undef) and ($sasl_distro != undef)) {
    if ($ldap_distro == $sasl_distro) {
    # Both defined and equal
      su_apt::source { $ldap_distro:
        comment       => 'Special sources.list.d file for OpenLDAP and Cyrus SASL',
        archive       => $ldap_debian_archive,
        distributions => [$ldap_distro],
        repository    => $ldap_debian_repository,
        notify        => Exec['ldap_aptitude_update'];
      }
    } else {
      # Both defined and UNequal
      su_apt::source { $sasl_distro:
        comment       => 'Special sources.list.d file for Cyrus SASL',
        archive       => $sasl_debian_archive,
        distributions => [$sasl_distro],
        repository    => $sasl_debian_repository,
        notify        => Exec['ldap_aptitude_update'];
      }
      su_apt::source { $ldap_distro:
        comment       => 'Special sources.list.d file for OpenLDAP',
        archive       => $ldap_debian_archive,
        distributions => [$ldap_distro],
        repository    => $ldap_debian_repository,
        notify        => Exec['ldap_aptitude_update'];
      }
    }
  }

  ## Pin some pacakges

  # Is $sasl_debian_distribution an "Archive" or "Codename"? If it's an Archive we
  # say
  #    Pin: release a=archive-name
  #
  # If it's a Codename we way
  #
  #    Pin: release n=code-name
  #
  # (See the apt_preferences man page for more information.)
  #
  # The way we differentiate is that only these strings are considered to be "Archives"
  #
  #   - unstable
  #   - stable
  #   - oldstable
  #   - oldoldstable
  #   - testing
  #
  # Everything else is a Codename.

  # The templates use $sasl_darg and $ldap_darg.
  case $ldap_debian_distribution {
    'unstable',
    'stable',
    'oldstable',
    'oldoldstable',
    'testing': { $ldap_darg = "a" }
    default:   { $ldap_darg = "n" }
  }

  case $sasl_debian_distribution {
    'unstable',
    'stable',
    'oldstable',
    'oldoldstable',
    'testing': { $sasl_darg = "a" }
    default:   { $sasl_darg = "n" }
  }

  if ($ldap_distro != undef) {
    # Pin some OpenLDAP packages.
    file { '/etc/apt/preferences.d/ldap':
      content => template('su_ldap/etc/apt/preferences.d/ldap.erb'),
      notify  => Exec['ldap_aptitude_update'];
    }
  }

  if ($sasl_distro != undef) {
    # Pin some Cyrus SASL packages.
    file { '/etc/apt/preferences.d/sasl':
      content => template('su_ldap/etc/apt/preferences.d/sasl.erb'),
      notify  => Exec['ldap_aptitude_update'];
    }
  }

  if (($ldap_distro != undef) or ($sasl_distro != undef)) {
    # Update aptitude.
    exec { 'ldap_aptitude_update':
      path        => '/usr/bin:/usr/sbin:/bin',
      command     => 'aptitude update',
      refreshonly => true,
    }
  }

}
