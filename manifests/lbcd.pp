# Setup lbcd for LDAP servers (LBDNS)
class su_ldap::lbcd (
  Enum['present', 'absent'] $ensure,
) inherits lbcd {
  if ($ensure == 'present') {
    ## PRESENT
    Lbcd::Default['/etc/default/lbcd'] {
      ensure      => present,
      daemon_opts => '-c /usr/bin/lb-slapd',
    }

    # Don't start lbcd if we aren't starting ldap.  And don't start lbcd
    # if we don't want the server to take queries.
    Service['lbcd'] {
      ensure => running,
      status => 'test -f /etc/noldap || test -f /etc/nolbcd || pidof lbcd'
    }
  } else {
    ## ABSENT
    Lbcd::Default['/etc/default/lbcd'] {
      ensure => 'absent',
    }
  }

}
