# Manage the sync scripts and their configurations.

# The reason that each one is selected seperately is that not all
# environments get the same sync scripts. While these are intended to be
# installed on the tools server, this class is still flexible enough to be
# used to install the scripts on an LDAP master.

# $ensure: set to 'absent' to remove ALL sync scripts. This parameter
# overrides all the other "enable_*" parameters; that is, if $ensure is
# set to 'absent', no matter what the $enable_* parameters are set to no
# sync scripts will be installed, and any that are present will be
# removed.


class su_ldap::sync_scripts (
  $env                                = undef,
  Enum['present', 'absent'] $ensure   = 'present',
  String                    $basedir  = '/etc/ldapadmin',
  #
  String $ldap_master_fqdn            = undef,
  #
  # Select the sync scripts one-by-one in the hiera file.
  $enable_ldap_wg_maint               = false,
  $enable_ldap_group_maint            = false,
  $enable_ldap_sync_sugal_attributes  = false,
  $enable_ldap_sync_suprivilegegroup  = false,
  $enable_ldap_alias_maint            = false,
  $enable_posix_account_sync          = false,
  $enable_ldap_reports                = false,
) {

  # We want a boolean version of $ensure.
  if ($ensure == 'present') {
    $ensure_b = true
  } else {
    $ensure_b = false
  }

  # Set up the common components needed by all sync scripts.
  class { 'su_ldap::sync_scripts::common':
    ensure  => $ensure,
    env     => $env,
    basedir => $basedir,
  }

  # put the conf file for the remctl smtp-allow (ldap-smtp-allow) here,
  # even though its package (libstanford-ldap-sync-scripts-perl)
  # is in sync_script::common, because we need $ldap_master_fqdn  
  file {
    "${basedir}/ldap-smtp-allow.conf":
      ensure  => $ensure,
      content => template('su_ldap/etc/ldapadmin/ldap-smtp-allow.conf.erb'),
      require => File[$basedir];
  }

  # We need this below.
  $ticket_file_path = "/var/run/ldap-sync-$env.tkt"

  # We need to define some basic stuff first, then pass it to some of
  # the classes below.
  case $env {
    'prod' : {
      $lsdb_host    = 'lsdb.stanford.edu'
      $generate_gid = '0'
    }
    default: {
      $lsdb_host    = 'lsdb-$env.stanford.edu'
      $generate_gid = '1'
    }
  }

  # NOTE: The special function su_ldap::ensure_from_boolean used below
  # simply converts a boolean to an ensure string: true goes to
  # 'present', false goes to 'absent'. The code for this function is in
  # this module in lib/puppet/functions/su_ldap/ensure_from_boolean.rb.

  ## SYNC SERVICE: ldap-wg-maint
  class { 'su_ldap::sync_scripts::ldap_wg_maint':
    ensure  => su_ldap::ensure_from_boolean($enable_ldap_wg_maint and $ensure_b),
    env     => $env,
    basedir => $basedir,
    ldap_master_fqdn => $ldap_master_fqdn,
    ticket_file_path => $ticket_file_path,
  }

  ## SYNC SERVICE: ldap-group-maint
  class { 'su_ldap::sync_scripts::ldap_group_maint':
    ensure  => su_ldap::ensure_from_boolean($enable_ldap_group_maint and $ensure_b),
    env     => $env,
    basedir => $basedir,
    ldap_master_fqdn => $ldap_master_fqdn,
    generate_gid     => $generate_gid,
  }

  ## SYNC SRVICE: ldap-sync-sugal-attributes
  class { 'su_ldap::sync_scripts::ldap_sync_sugal_attributes':
    ensure  => su_ldap::ensure_from_boolean($enable_ldap_sync_sugal_attributes and $ensure_b),
    env     => $env,
    basedir => $basedir,
    ldap_master_fqdn => $ldap_master_fqdn,
  }

  ## SYNC SERVICE: ldap-sync-suprivilegegroup.service
  class { 'su_ldap::sync_scripts::ldap_sync_suprivilegegroup':
    ensure  => su_ldap::ensure_from_boolean($enable_ldap_sync_suprivilegegroup and $ensure_b),
    env     => $env,
    basedir => $basedir,
    ldap_master_fqdn => $ldap_master_fqdn,
  }

  ## SYNC SERVICE: ldap-alias-maint
  class { 'su_ldap::sync_scripts::ldap_alias_maint':
    ensure  => su_ldap::ensure_from_boolean($enable_ldap_alias_maint and $ensure_b),
    env     => $env,
    ldap_master_fqdn => $ldap_master_fqdn,
  }

  # posix accounts syncing
  $ensure_posix_account_sync = su_ldap::ensure_from_boolean($enable_posix_account_sync and $ensure_b)

  su_ldap::sync_scripts::posix_account_sync { 'aeroastroarl':
    ensure           => $ensure_posix_account_sync,
    env              => $env,
    ldap_master_fqdn => $ldap_master_fqdn,
    basedir          => $basedir,
  }
  su_ldap::sync_scripts::posix_account_sync { 'anesthesia':
    ensure           => $ensure_posix_account_sync,
    env              => $env,
    ldap_master_fqdn => $ldap_master_fqdn,
    basedir          => $basedir,
  }
  su_ldap::sync_scripts::posix_account_sync { 'statistics':
    ensure           => $ensure_posix_account_sync,
    env              => $env,
    ldap_master_fqdn => $ldap_master_fqdn,
    basedir          => $basedir,
  }

  # ldap_reports
  class { 'su_ldap::sync_scripts::ldap_reports':
    ensure           => su_ldap::ensure_from_boolean($enable_ldap_reports and $ensure_b),
    env              => $env,
    ldap_master_fqdn => $ldap_master_fqdn,
    addresses        => $addresses,
  }

}
