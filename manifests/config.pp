
# $basedir: where the OpenLDAP databases live. These databases can get quite large, so
#   sometimes set these up as separate disk partitions.
# Default: /var/lib/ldap
#
# $keytab_path: the path to the keytab file used for setting up the kerberos context
# (needed for GSSAPI authentication). This keytab is used to create the ticket cache
# specified by $krb5ccname.
#
# IMPORTANT!!! This class does NOT set up the service to generate the service ticket
# pointed to by $krb5ccname. You must do that in the calling class.

class su_ldap::config (
  $hosting_model = undef,
  $basedir       = '/var/lib/ldap',
  $keytab_path   = '/etc/krb5.keytab',
  $krb5ccname    = '/var/run/ldap_syncreplica.tkt',
  Boolean $auth_gssapi   = true,
  Boolean $auth_simple   = true,
  Boolean $ldap_over_tcp = true,
  Boolean $ldap_over_tls = true,
) {

  # Set up some directories including the ones where the databases
  # go.
  file {
    '/etc/keytab':
      ensure => directory,
      mode   => '0755';
    $basedir:
      ensure => directory,
      mode   => '0755';
    "${basedir}/accesslog":
      ensure => directory,
      mode   => '0755';
    '/var/run/slapd':
      ensure => directory;
  }

  # Make sure the directory that holds the cn=config ldif file exists.
  file { '/etc/ldap/slapd.d':
    ensure  => directory,
    mode    => '0755',
    require => Package['slapd'],
  }

  # Install /etc/ldap/ldap.conf.
  file { '/etc/ldap/ldap.conf':
    source => 'puppet:///modules/su_ldap/etc/ldap/ldap.conf',
    require => Package['slapd'],
  }

  # The /etc/default/slapd file specifies keytab locations, ticket caches,
  # startup and library preload options as well as other goodies. The file
  # will set the KRB5CCNAME environment variables if the host is a
  # replica.
  file { '/etc/default/slapd':
   content => template('su_ldap/etc/default/slapd.erb'),
  }

  # Sets up log directory.
  include su_ldap::config::logging

}
