##################################################################
# Munin class for LDAP servers.
#

class su_ldap::munin (
  $env              = '',
) {

  include munin::as_root

  case $env {
    'pre', 'sbx', 'wlu', 'dev', 'test', 'uat': {
      $munin_group_value  = 'LDAP-test'
    }
    'prod': {
      $munin_group_value  = 'LDAP'
    }
    default: {
      fail("unrecognized environment ${env}")
    }
  }
                                                                   
  facts::instance { 'su_munin_group': 
     value => $munin_group_value
  }

  # Munin monitoring
  file {
    '/etc/munin/plugin-conf.d/slapd':
      mode   => '0644',
      source => 'puppet:///modules/su_ldap/etc/munin/plugin-conf.d/slapd-bdb';
    '/etc/munin/plugins/slapd_bdb_cache_pages':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_bdb_cache_';
    '/etc/munin/plugins/slapd_bdb_cache_percent':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_bdb_cache_';
    '/etc/munin/plugins/slapd_connections':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_';
    '/etc/munin/plugins/slapd_operations':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_';
    '/etc/munin/plugins/slapd_operations_diff':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_';
    '/etc/munin/plugins/slapd_statistics_bytes':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_';
    '/etc/munin/plugins/slapd_statistics_pdu':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_';
    '/etc/munin/plugins/slapd_waiters':
      ensure => link,
      target => '/usr/share/munin/plugins/slapd_';
  }

}
