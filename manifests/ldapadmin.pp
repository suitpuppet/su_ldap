# Create the directory /etc/ldapadmin.

class su_ldap::ldapadmin (
  Enum['present', 'absent'] $ensure,
  #
  Enum['present', 'absent'] $ensure_mail_lists        = 'absent',
  Enum['present', 'absent'] $ensure_krb_audit         = 'absent',
  Enum['present', 'absent'] $ensure_log_store         = 'absent',
  Enum['present', 'absent'] $ensure_mail_destinations = 'absent',
  Enum['present', 'absent'] $ensure_ldap_serverctl    = 'absent',
  Enum['present', 'absent'] $ensure_ldap_watcher      = 'absent',
  Enum['present', 'absent'] $ensure_ldap_posix_group  = 'present',
  #
  Optional[String] $env = undef,
  #
  # $master is needed only for the mail_lists ldapadmin function.
  Optional[String] $master = undef,
) {

  if ($ensure == 'present') {
    case $env {
      'sbx', 'dev', 'test', 'uat', 'pre': {
        $db_name  = 'ldaplog_dev'
        $db_creds = 'dev'
      }
      'prod': {
        $db_name  = 'ldaplog'
        $db_creds = 'prod'
      }
    }
  }


  if ($ensure == 'present') {
    file { '/etc/ldapadmin':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
    }
  } else {
    file { '/etc/ldapadmin':
      ensure => absent,
    }
  }

  # Configuration for LDAPadmin.pm
  file { '/etc/ldapadmin/ldapadmin.conf':
    ensure  => $ensure,
    mode    => '0644',
    content => template('su_ldap/etc/ldapadmin/ldapadmin.conf.erb'),
    require => File['/etc/ldapadmin'],
  }

  # Get the database credentials used by LDAPadmin.pm
  #
  # These two credentials files would be combined into a
  # single file.  This will require changes to both the
  # applications and to the wallet objects themselves.
  #
  # Credentials for accessing the ldaplog database.
  base::wallet { "idg-ldapadmin-db-ldaplog-${db_creds}":
    ensure  => $ensure,
    path    => '/etc/ldapadmin/database.password',
    type    => 'file',
    owner   => root,
    group   => root,
    mode    => '0640',
    require => File['/etc/ldapadmin'],
  }

  # db credentials for the ldaplog database
  base::wallet { "idg-ldap-db-ldaplog-${db_creds}-password":
    ensure  => $ensure,
    path    => '/etc/ldapadmin/ldap-log-store.db',
    type    => file,
    require => File['/etc/ldapadmin']
  }

  # The help file
  file { '/etc/ldapadmin/help.pod':
    ensure  => $ensure,
    mode    => '0644',
    source  => 'puppet:///modules/su_ldap/etc/ldapadmin/help.pod',
    require => File['/etc/ldapadmin'],
  }

  # NOTE: This interface appears NOT to be used anymore, but we leave the
  # Puppet code in place for now.
  # The mail-lists interface.
  class { 'su_ldap::ldapadmin::mail_lists':
    ensure => $ensure_mail_lists,
    master => $master,
  }

  # The Kerberos audit script.
  class { 'su_ldap::ldapadmin::krb_audit':
    ensure => $ensure_krb_audit,
  }

  # Store OpenLDAP logs in a database.
  class { 'su_ldap::ldapadmin::log_store':
    ensure  => $ensure_log_store,
    db_name => $db_name,
  }

  # Store suMailDrop attributes in a MySQL database.
  class { 'su_ldap::ldapadmin::mail_destinations':
    ensure => $ensure_mail_destinations,
  }

  # Used to control other LDAP servers (not used?).
  class { 'su_ldap::ldapadmin::ldap_serverctl':
    ensure => $ensure_ldap_serverctl,
  }

  # The LDAP accesslog watcher.
  class { 'su_ldap::ldapadmin::ldap_watcher':
    ensure => $ensure_ldap_watcher,
  }

  # LDAP posixgroup (ldap-posix-group)
  class { 'su_ldap::ldapadmin::posixgroup':
    env    => $env,
    ensure => $ensure_ldap_posix_group,
  }

}
