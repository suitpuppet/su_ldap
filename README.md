[[_TOC_]]

# Stanford ACS OpenLDAP Puppet Module

Puppet module to manage Stanford's OpenLDAP service. Supports both
traditional servers and containers.

## OpenLDAP Installation and Configuration

### OpenLDAP Version

The default is to install whichever version of
OpenLDAP `apt` thinks should be installed. This will, of course, depend on
the version of Debian underlying the host.

However, you can override this by using the `install_archive` and
`install_distro`. If the distribution you want to use is already in
included in the APT sources via `/etc/apt/sources.list.d` then use
`install_distribution` to specify the distribution:

```puppet
# Use the "testing" distribution
class { 'ldap':
  install_distribution => 'testing',
}
```

If the distribution is _not_ already included in `/etc/apt/sources.list.d`
then you can use `install_repository` to add a `.list` file to
`/etc/apt/sources.list.d` that tells `apt` where to look for the
distribution. For example,

```puppet
# Use the "stretch-prod" distribution.
class { 'ldap':
  install_distribution => 'stretch-prod',
  install_repository   => 'https://debian.stanford.edu/debian-stanford',
}
```

### Installation

The main class `ldap` supports the installation of OpenLDAP and some of
OpenLDAP's configuration. The simplest installation (which accepts all the
defaults) is:

```puppet
class { 'ldap':
    }
```

This assumes installation on a traditional Debian server and will install
whichever version of OpenLDAP is current for that version of Debian.

### Hosting Model

If you are installing onto a container, use the parameter `hosting_model`
and set to `container`:

```puppet
  class { 'ldap':
    hosting_model => 'container',
  }
```

The only two acceptable values for `hosting_model` are `traditional` and
`container`.


## Schema Files

We keep the definitive source for Stanford's LDAP schema files in the
directory `files/schema`.

## Puppet update

### Sync External Repo changes to Puppet repository

Puppet repository didn't know the changes we did to the external repo changes. So we need to make a trivial change to Puppetfile to pull all the external repo changes.

Example: After making changes to external repository, we need to edit *Puppetfile* in
[idg puppet repo](https://uitgitaws.amazon.stanford.edu/idg/puppet.git) and push the changes.

```
 Before:
    ## Puppetfile - Sync!!

 After: 
    ## Puppetfile - Sync!
```

### Check sync status

Once you make change, check git repo sync changes against puppet server. It may take a few minutes
for the sync to complete:

```shell
$ remctl puppetservice5 git-info idg_master
...
```

### Run oneshot to verify the change

```shell
# As root principal
$ remctl <ldap-env3>|<ldap3> pupppet oneshot
...
```
