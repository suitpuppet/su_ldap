# Convert a boolean to one of "present" or "absent". A true value goes
# to "present" while a false value goes to "absent".
Puppet::Functions.create_function(:'su_ldap::ensure_from_boolean') do
  def ensure_from_boolean(boolean_value)
    if (boolean_value)
      return 'present'
    else
      return 'absent'
    end
  end
end
